#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include "gol.h"

int main() {
    int winx, winy;
    initscr();
    noecho();
    curs_set(FALSE);
    getmaxyx(stdscr, winy, winx);
    game_grid *thing = create(winy-1,winy-1);
    for (int i = 5; i < 10; ++i)
        for (int j = 5; j < 10; ++j)
            set(thing,i,j,1);

    while (1) {
        char *print = strgrd(thing);
        mvprintw(0,0,print);
        refresh();
        free(print);
        usleep(300000);
        clear();
        advgame(thing);
    }

    endwin();
}
