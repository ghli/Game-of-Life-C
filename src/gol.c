#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include "gol.h"
game_grid* create(int l, int h)
{
    game_grid *t = malloc(sizeof(game_grid));
    t->l=l;
    t->h=h;
    t->lh=malloc(l*h*sizeof(short));
    t->slh=malloc(l*h*sizeof(short));
    return t;
}

int advgrdlc(game_grid *g, int x, int y)
{
    int c=0;
    for (int i=-1; i<2; ++i)
        for (int j=-1; j<2; ++j)
            if((i!=0||j!=0)&&get(g,x+i,y+j))
                ++c;
    
    if(c<2||c>3) {
        printf("%d,%d|",x,y);
        setshdw(g, x, y, 0);
    }
    else if (c==3)
        setshdw(g, x, y, 1);
    return c;
}

void swpshdw(game_grid *g) {
    unsigned short *t = g->lh;
    g->lh = g->slh;
    g->slh = t;
    memcpy(g->slh,g->lh,g->l*g->h*sizeof(short));
}

void advgame(game_grid *g) {
    for (int i=0; i<g->l; ++i)
        for (int j=0; j<g->h; ++j)
            advgrdlc(g,i,j);
    swpshdw(g);
}

unsigned short get(game_grid *g, int x, int y)
{
    if(x<0||x>=g->l||y>=g->h||y<0)
        return 0;
    return g->lh[y*g->l+x];
}

void set(game_grid *g, int x, int y, unsigned short v)
{
    if(x<0||x>=g->l||y>=g->h||y<0)
        return;
    g->lh[y*g->l+x]=v;
    setshdw(g,x,y,v);
}

void setshdw(game_grid *g, int x, int y, unsigned short v)
{
    if(x<0||x>=g->l||y>=g->h||y<0)
        return;
    g->slh[y*g->l+x]=v;
}

void prntgrd(game_grid *g) {
    for (int i=0; i<g->l; ++i) {
        for(int j=0; j<g->h; ++j)
            printf("%d ",get(g, i, j));
        printf("\n");
    }
}

char* strgrd(game_grid *g) {
    char *tmp = malloc(sizeof(char)*g->l*g->h*2);
    bool f = true;
    for (int j = 0; j < g->h; ++j)
        for (int i = 0; i < g->l; ++i) {
            tmp[2*j*g->l+2*i] = get(g,i,j)?'+':' ';
            if (i < g->l-1)
                tmp[2*j*g->l+2*i+1] = ' ';
            else
                tmp[2*j*g->l+2*i+1] = '\n';
        }
    tmp[sizeof(char)*g->l*g->h*2-1] = '\0';
    return tmp;
}

void destroy(game_grid *g) {
    free(g->lh);
    free(g->slh);
    free(g);
}



