typedef struct grid{
    unsigned int l;
    unsigned int h;
    unsigned short *lh;
    unsigned short *slh;
} game_grid;

game_grid* create(int, int);
int advgrdlc(game_grid*, int, int);
void swpshdw(game_grid*);
void advgame(game_grid*);
unsigned short get(game_grid*, int, int);
void set(game_grid*, int, int, unsigned short);
void setshdw(game_grid*, int, int, unsigned short);
void prntgrd(game_grid*);
char* strgrd(game_grid*);
void destroy(game_grid*);


